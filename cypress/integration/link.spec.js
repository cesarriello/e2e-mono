context('Actions', () => {
  beforeEach(() => {
    cy.visit('https://www.itau.com.br')
  })
  
  it('.type() - client cpf', () => {
    cy.get('[href="http://www.itau.com.br/cartoes"]').click()
    cy.wait(1000)
    
    cy.get('#menuTypeAccess').click()
    
    cy.get('li[data-select-form=cpf]').click()
    
    cy.wait(1000)
    cy.get('#cpf').type('27982348831')
    
    cy.get('#btnLoginSubmit').click()
    
    cy.wait(1000)
    cy.get('#tecla_0').click()
  })

})
