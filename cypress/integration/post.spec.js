context('Actions', () => {
  beforeEach(() => {
  })
  
  it('.type() - client cpf', () => {
    // cy.visit({
    //   url: 'https://internetnc.itau.com.br/router-app/router',
    //   method: 'POST',
    //   body: {
    //     'usuario.cpf': '27982348831',
    //     'portal': '999',
    //     'pre-login': 'pre-login',
    //     'destino': '',
    //     'tipoLogon': '9',
    //   }
    // })

    // cy.visit({
    //   url: 'https://internetnc2.itau.com.br/router-app/router',
    //   method: 'POST',
    //   body: {
    //     'usuario.cpf': '27982348831',
    //     'portal': 999,
    //     'pre-login': 'pre-login',
    //     'destino': '',
    //     'tipoLogon': 9,
    //   }
    // })

    cy.visit({
      url: 'https://internetnc2.itau.com.br/router-app/router',
      method: 'POST',
      body: 'usuario.cpf=27982348831&portal=999&pre-login=pre-login&destino=&tipoLogon=9'
    })

    cy.wait(1000)
    
    cy.get('#menuTypeAccess').click()
    
    cy.get('li[data-select-form=cpf]').click()
    
    cy.wait(1000)
    cy.get('#cpf').type('27982348831')
    
    cy.get('#btnLoginSubmit').click()
    
    cy.wait(1000)
    cy.get('#tecla_0').click()
  })

})
