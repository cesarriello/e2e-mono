/// <reference types="Cypress" />

context('Actions', () => {
  beforeEach(() => {
    cy.visit('https://itau.com.br')
    // cy.visit('https://www.itau.com.br/cartoes')
  })
  
  it('.type() - client cpf', () => {
    // cy.get('[href="http://www.itau.com.br/cartoes"]').click()
    cy.visit('https://www.itau.com.br/cartoes')
    cy.wait(1000)
    
    cy.get('#menuTypeAccess').click()
    
    cy.get('li[data-select-form=cpf]').click()
    
    cy.wait(1000)
    cy.get('#cpf').type('27982348831')
    
    cy.get('#btnLoginSubmit').click()
    
    cy.wait(1000)
    cy.get('#tecla_0').click()
  })

//   CypressError: Cypress detected a cross origin error happened on page load:

//   > Blocked a frame with origin "https://www.itau.com.br" from accessing a cross-origin frame.

// Before the page load, you were bound to the origin policy:
//   > https://itau.com.br

// A cross origin error happens when your application navigates to a new superdomain which does not match the origin policy above.

// This typically happens in one of three ways:

// 1. You clicked an <a> that routed you outside of your application
// 2. You submitted a form and your server redirected you outside of your application
// 3. You used a javascript redirect to a page outside of your application

// Cypress does not allow you to change superdomains within a single test.

// You may need to restructure some of your test code to avoid this problem.

// Alternatively you can also disable Chrome Web Security which will turn off this restriction by setting { chromeWebSecurity: false } in your 'cypress.json' file.

// https://on.cypress.io/cross-origin-violation



})
